
function aboutGameUse() {
    
    if ($(window).width() >= 1920) {
        $('.js-about-game-item').on('click', function () {
            var $thisItem = $(this),
                $thisContent = $thisItem.find('.about-game__content'),
                $thisBtn = $thisItem.find('.js-btn-more');
        
            $thisItem.toggleClass('is-active-item');
            $thisContent.fadeToggle(200);
        
            if ($thisItem.hasClass('is-active-item')) {
                $thisBtn.text('Close').addClass('is-active-btn');
            } else {
                $thisBtn.text('Learn more').removeClass('is-active-btn');
            }
        
            $thisItem.siblings().removeClass('is-active-item').find('.about-game__content').hide();
            $thisItem.siblings().find('.js-btn-more').text('Learn more').removeClass('is-active-btn');
            $thisItem.siblings().addClass('is-not-active-item')
            $thisItem.removeClass('is-not-active-item');
        
            if (!$thisItem.hasClass('is-active-item')) {
                $thisItem.removeClass('is-not-active-item');
            }
            
        });
    }
} 

aboutGameUse();

function initSlider() {
    if ($(window).width() <= 1919) {

        var slider = $(".js-mobile-slider");
        var scrollCount = null;
        var scroll = null;
        slider
            .slick({
                dots: false,
                arrows: false,
                vertical: true,
                // infinite: false,
                slidesToShow: 1,
                slidesToScroll: 1,
                verticalSwiping: true,
                swipeToSlide: true,
            });
        
        slider.on('wheel', (function (e) {
            e.preventDefault();
        
            clearTimeout(scroll);
            scroll = setTimeout(function () { scrollCount = 0; }, 200);
            if (scrollCount) return 0;
            scrollCount = 1;
        
            if (e.originalEvent.deltaY < 0) {
                $(this).slick('slickNext');
            } else {
                $(this).slick('slickPrev');
            }
        }));
    }
}

initSlider();